package com.v2px.sujin.contactvg.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.v2px.sujin.contactvg.dao.ContactDAO;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;

@Database(entities = {Contacts.class}, version = 1)
public abstract class ContactDatabase extends RoomDatabase{

    private static final String DB_NAME = "contactDatabase.db";
    private static ContactDatabase instance;

    public static synchronized ContactDatabase getInstance(Context context) {
        if(instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static ContactDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                ContactDatabase.class,
                DB_NAME
        ).build();
    }

    public abstract ContactDAO getContactDAO();
}
