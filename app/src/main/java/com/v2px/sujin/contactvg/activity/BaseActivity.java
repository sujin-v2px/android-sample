package com.v2px.sujin.contactvg.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.v2px.sujin.contactvg.MyApplication;
import com.v2px.sujin.contactvg.network.APIService;
import com.v2px.sujin.contactvg.network.RestClient;

public class BaseActivity extends AppCompatActivity {

    public static APIService apiService;
    public static MyApplication myApplication;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RestClient restClient = new RestClient();
        apiService = restClient.getApiService();
        myApplication = (MyApplication) getApplication();
    }
}
