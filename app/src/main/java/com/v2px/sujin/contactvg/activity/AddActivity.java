package com.v2px.sujin.contactvg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.v2px.sujin.contactvg.R;
import com.v2px.sujin.contactvg.constants.Util;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;
import com.v2px.sujin.contactvg.network.CheckConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * For simplicity, I added the image using a static URL instead of uploading file.
 */

public class AddActivity extends BaseActivity {

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etPhone)
    EditText etPhone;

    @BindView(R.id.etAddress)
    EditText etAddress;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

    }

    @OnClick({R.id.btnAdd})
    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                if(CheckConnection.checkNetworkAvailability(this)) {
                    insertData();
                } else {
                    Toast.makeText(this, "Internet Connection Unavailable", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void insertData() {
        Contacts contacts = new Contacts();
        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();
        String address = etAddress.getText().toString();

        if(Util.isEmpty(name)){
            etName.setError("Required");
            return;
        }

        if(Util.isEmpty(phone)) {
            etPhone.setError("Required");
            return;
        }

        if(Util.isEmpty(address)) {
            etAddress.setError("Required");
            return;
        }

        contacts.setFullName(name);
        contacts.setAddress(address);
        contacts.setPhoneNumber(phone);
        contacts.setAvatarUrl("https://robohash.org/liberosapientequia.png?size=100x100&set=set1");

        Observable<Contacts> contactsObservable = apiService.insertContact(contacts);
        contactsObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item)
                .subscribe(new Observer<Contacts>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Contacts contacts) {
                        Intent returnIntent = getIntent();
                        setResult(RESULT_CANCELED, returnIntent);
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
