package com.v2px.sujin.contactvg.network;


import com.v2px.sujin.contactvg.entity.contacts.Contacts;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface APIService {

    @GET("Contacts")
    Observable<Contacts[]> getContacts();

    @POST("Contacts")
    Observable<Contacts> insertContact(@Body Contacts contacts);

    @PUT("Contacts/{objectId}")
    Observable<Contacts> updateContact(@Path("objectId") String objectId, @Body Contacts contacts);

    @DELETE("Contacts/{objectId}")
    Observable<Contacts> deleteContact(@Path("objectId") String objectId);
}
