package com.v2px.sujin.contactvg.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.v2px.sujin.contactvg.R;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailActivity extends BaseActivity {

    Contacts contact;

    @BindView(R.id.imgAvatar)
    ImageView imgAvatar;

    @BindView(R.id.txtName)
    TextView txtName;

    @BindView(R.id.txtPhone)
    TextView txtPhone;

    @BindView(R.id.txtAdderss)
    TextView txtAddress;

    @BindView(R.id.btnEdit)
    FloatingActionButton btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        contact = Parcels.unwrap(getIntent().getParcelableExtra("contact"));

        setData();

        btnEdit.setOnClickListener(view -> {
            Intent intent = new Intent(ContactDetailActivity.this, EditActivity.class);
            intent.putExtra("contact", Parcels.wrap(contact));
            startActivityForResult(intent, 1);
        });
    }

    private void setData() {
        txtName.setText(contact.getFullName());
        txtAddress.setText(contact.getAddress());
        txtPhone.setText(contact.getPhoneNumber());
        Glide.with(this).load(contact.getAvatarUrl()).into(imgAvatar);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1) {
            if(resultCode == Activity.RESULT_OK) {
                contact = Parcels.unwrap(data.getParcelableExtra("contact"));
                setData();
            }
        }
    }
}
