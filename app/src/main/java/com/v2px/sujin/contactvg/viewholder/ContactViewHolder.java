package com.v2px.sujin.contactvg.viewholder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.v2px.sujin.contactvg.R;

import butterknife.ButterKnife;

public class ContactViewHolder extends RecyclerView.ViewHolder {

    public TextView txtName, txtPhone;

    public ImageView imgAvatar;

    public ContactViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);
        txtName = itemView.findViewById(R.id.txtName);
        txtPhone = itemView.findViewById(R.id.txtPhone);
        imgAvatar = itemView.findViewById(R.id.imgAvatar);
    }
}
