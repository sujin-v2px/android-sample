package com.v2px.sujin.contactvg.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.v2px.sujin.contactvg.R;
import com.v2px.sujin.contactvg.adapter.ContactAdapter;
import com.v2px.sujin.contactvg.dao.ContactDAO;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;
import com.v2px.sujin.contactvg.network.CheckConnection;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends BaseActivity {

    @BindView(R.id.contactRecyclerView)
    RecyclerView contactRecyclerView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.btnAdd)
    FloatingActionButton btnAdd;

    Context context;
    ContactDAO contactDAO;
    Contacts[] contactData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initView();

        connection();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                connection();
            }
        });

    }

    @OnClick({R.id.btnAdd})
    public void OnButtonClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                startActivityForResult(
                        new Intent(getApplicationContext(), AddActivity.class),
                        1);

        }
    }


    private void connection() {
        if (CheckConnection.checkNetworkAvailability(getApplicationContext())) {
            fetchData();
        } else {
            showProgress();
            getDataOffline();
            hideProgress();
        }
    }

    private void initView() {
        context = getApplicationContext();
        contactRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        contactDAO = myApplication.getContactDatabase().getContactDAO();
    }

    private void getDataOffline() {
        Observable<List<Contacts>> contactsList = Observable.fromCallable(() -> contactDAO.getAllContacts());
        contactsList
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item)
                .subscribe(contacts -> {
                    Contacts[] c = new Contacts[contacts.size()];
                    setContactAdapter(contacts.toArray(c));
                });
    }

    private void fetchData() {
        showProgress();

        Observable<Contacts[]> contactListObservable = apiService.getContacts();

        contactListObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item)
                .subscribe(new Observer<Contacts[]>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Contacts[] contacts) {
                        contactData = contacts;
                        setContactAdapter(contacts);

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Error", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        hideProgress();
                        insertDataInDB(contactData);
                    }
                });

    }

    private void insertDataInDB(Contacts[] contacts) {
        Thread thread = new Thread(() -> contactDAO.insert(contacts));
        thread.start();

    }

    private void setContactAdapter(Contacts[] contacts) {
        ContactAdapter contactAdapter = new ContactAdapter(context, contacts);
        contactRecyclerView.setAdapter(contactAdapter);
        contactAdapter.setOnContactListener((contactItem, position) -> {
            Intent intent = new Intent(context, ContactDetailActivity.class);
            intent.putExtra("contact", Parcels.wrap(contactItem));
            startActivity(intent);
        });
    }

    private void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
        contactRecyclerView.setVisibility(View.GONE);
        btnAdd.setVisibility(View.GONE);
    }

    private void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
        contactRecyclerView.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        connection();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                onRestart();
            }
        }
    }
}
