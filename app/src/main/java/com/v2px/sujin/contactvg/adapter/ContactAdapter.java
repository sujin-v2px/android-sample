package com.v2px.sujin.contactvg.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.v2px.sujin.contactvg.R;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;
import com.v2px.sujin.contactvg.entity.contacts.OnContactListener;
import com.v2px.sujin.contactvg.viewholder.ContactViewHolder;

public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    private Context context;
    private Contacts[] contactList;
    private OnContactListener onContactListener;

    public ContactAdapter(Context context, Contacts[] contactList) {
        this.context = context;
        this.contactList = contactList;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_contact, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contacts contactItem = contactList[position];
        holder.txtName.setText(contactItem.getFullName());
        holder.txtPhone.setText(contactItem.getPhoneNumber());
        Glide.with(context)
                .load(contactItem.getAvatarUrl())
                .into(holder.imgAvatar);
        holder.itemView.setOnClickListener(view -> onContactListener.onContactClick(contactItem, position));
    }

    @Override
    public int getItemCount() {
        return contactList.length;
    }

    public void setOnContactListener(OnContactListener onContactListener) {
        this.onContactListener = onContactListener;
    }
}
