package com.v2px.sujin.contactvg.entity.contacts;


public interface OnContactListener {
    void onContactClick(Contacts contactItem, int position);
}
