package com.v2px.sujin.contactvg.schedular;


import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import com.v2px.sujin.contactvg.dao.ContactDAO;
import com.v2px.sujin.contactvg.database.ContactDatabase;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;
import com.v2px.sujin.contactvg.network.APIService;
import com.v2px.sujin.contactvg.network.RestClient;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ContactJobService extends JobService {

    private static final String TAG = ContactJobService.class.getSimpleName();
    boolean isWorking = false;
    boolean jobCancelled = false;

    private ContactDAO contactDAO;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "JOB Started");
        isWorking = true;
        getDataInBackground(params);
        return isWorking;
    }

    private void getDataInBackground(JobParameters params) {
        contactDAO = ContactDatabase.getInstance(getApplicationContext()).getContactDAO();
        RestClient restClient = new RestClient();
        APIService apiService = restClient.getApiService();
        Observable<Contacts[]> contactListObservable = apiService.getContacts();

        contactListObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item)
                .subscribe(new Observer<Contacts[]>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Contacts[] contacts) {
                        if(jobCancelled) return;
                        insertDataInDB(contacts);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Error", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "Job Completed");
                        isWorking = false;
                        boolean needsReschedule = false;
                        jobFinished(params, needsReschedule);
                    }
                });

    }

    private void insertDataInDB(Contacts[] contacts) {
        Thread thread = new Thread(() -> contactDAO.insert(contacts));
        thread.start();

    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "Job cancelled");
        jobCancelled = true;
        boolean needsReschedule = isWorking;
        jobFinished(params, needsReschedule);
        return needsReschedule;
    }
}
