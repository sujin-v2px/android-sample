package com.v2px.sujin.contactvg.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.v2px.sujin.contactvg.R;
import com.v2px.sujin.contactvg.constants.Util;
import com.v2px.sujin.contactvg.entity.contacts.Contacts;
import com.v2px.sujin.contactvg.network.CheckConnection;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class EditActivity extends BaseActivity {

    Contacts contact;

    @BindView(R.id.imgAvatar)
    ImageView imgAvatar;

    @BindView(R.id.etName)
    EditText etname;

    @BindView(R.id.etPhone)
    EditText etPhone;

    @BindView(R.id.etAddress)
    EditText etAddress;

    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.btnDelete)
    Button btnDelete;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.rootLayout)
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        contact = Parcels.unwrap(getIntent().getParcelableExtra("contact"));

        ButterKnife.bind(this);

        initData();

    }

    private void initData() {
        Glide.with(this).load(contact.getAvatarUrl()).into(imgAvatar);
        etname.setText(contact.getFullName());
        etPhone.setText(contact.getPhoneNumber());
        etAddress.setText(contact.getAddress());
    }

    @OnClick({R.id.btnSave, R.id.btnDelete})
    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (CheckConnection.checkNetworkAvailability(this)) {
                    updateContact();
                } else {
                    Toast.makeText(this, "Internet Connection Unavailable", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnDelete:
                if (CheckConnection.checkNetworkAvailability(this)) {
                    deleteContact();
                } else {
                    Toast.makeText(this, "Internet Connection Unavailable", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void updateContact() {
        showProgessBar();
        String name = etname.getText().toString();
        String phone = etPhone.getText().toString();
        String address = etAddress.getText().toString();

        if(Util.isEmpty(name)){
            etname.setError("Required");
            return;
        }

        if(Util.isEmpty(phone)) {
            etPhone.setError("Required");
            return;
        }

        if(Util.isEmpty(address)) {
            etAddress.setError("Required");
            return;
        }

        contact.setFullName(name);
        contact.setPhoneNumber(phone);
        contact.setAddress(address);

        Observable<Contacts> contactsObservable = apiService.updateContact(
                contact.getObjectId(),
                contact);

        contactsObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item)
                .subscribe(new Observer<Contacts>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Contacts c) {
                        contact = c;
                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("contact", Parcels.wrap(c));
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        hideProgressBar();
                    }
                });
    }

    private void deleteContact() {
        showProgessBar();
        Observable<Contacts> contactsObservable = apiService.deleteContact(contact.getObjectId());
        contactsObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(item -> item)
                .subscribe(new Observer<Contacts>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Contacts contacts) {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        hideProgressBar();
                    }
                });
    }

    private void showProgessBar() {
        progressBar.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
    }

}
