package com.v2px.sujin.contactvg;


import android.app.Application;

import com.v2px.sujin.contactvg.database.ContactDatabase;

public class MyApplication extends Application {

    private ContactDatabase contactDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        contactDatabase = ContactDatabase.getInstance(this);
    }

    public ContactDatabase getContactDatabase() {
        return contactDatabase;
    }
}
