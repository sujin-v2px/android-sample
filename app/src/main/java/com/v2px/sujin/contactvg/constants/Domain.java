package com.v2px.sujin.contactvg.constants;


public class Domain {
    public static String BASE_URL =
            "https://api.backendless.com/"+
                    BackendlessConstant.APP_ID+"/"+
                    BackendlessConstant.ANDROID_API_KEY+"/data/";
}
