package com.v2px.sujin.contactvg.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.v2px.sujin.contactvg.entity.contacts.Contacts;

import java.util.List;

@Dao
public interface ContactDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Contacts... contacts);

    @Update
    void update(Contacts... contacts);

    @Delete
    void delete(Contacts... contacts);

    @Query("SELECT * FROM contacts")
    List<Contacts> getAllContacts();

}
