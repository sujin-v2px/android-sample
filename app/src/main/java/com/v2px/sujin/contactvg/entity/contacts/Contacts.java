package com.v2px.sujin.contactvg.entity.contacts;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Entity
@Parcel
public class Contacts {

    @PrimaryKey
    @NonNull
    @SerializedName("objectId")
    private String objectId;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("address")
    private String address;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("created")
    private long created;

    @SerializedName("___class")
    private String className;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("ownerId")
    private String ownerId;

    @SerializedName("updated")
    private long updated;

    @SerializedName("deletionTime")
    private long deletionTime;

    @SerializedName("email")
    private String email;

    @SerializedName("__meta")
    private String meta;

    public Contacts() {

    }

    public Contacts(long deletionTime) {
        this.deletionTime = deletionTime;
    }

    public Contacts(String fullName, String address, String avatarUrl, long created, String className, String phoneNumber, String ownerId, long updated, String objectId, String email, String meta) {
        this.fullName = fullName;
        this.address = address;
        this.avatarUrl = avatarUrl;
        this.created = created;
        this.className = className;
        this.phoneNumber = phoneNumber;
        this.ownerId = ownerId;
        this.updated = updated;
        this.objectId = objectId;
        this.email = email;
        this.meta = meta;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getCreated() {
        return created;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public long getUpdated() {
        return updated;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getMeta() {
        return meta;
    }

    public long getDeletionTime() {
        return deletionTime;
    }

    public void setDeletionTime(long deletionTime) {
        this.deletionTime = deletionTime;
    }

    @Override
    public String toString() {
        return
                "Contacts{" +
                        "full_name = '" + fullName + '\'' +
                        ",address = '" + address + '\'' +
                        ",avatar_url = '" + avatarUrl + '\'' +
                        ",created = '" + created + '\'' +
                        ",___class = '" + className + '\'' +
                        ",phone_number = '" + phoneNumber + '\'' +
                        ",ownerId = '" + ownerId + '\'' +
                        ",updated = '" + updated + '\'' +
                        ",objectId = '" + objectId + '\'' +
                        ",email = '" + email + '\'' +
                        ",__meta = '" + meta + '\'' +
                        "}";
    }
}