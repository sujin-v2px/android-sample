# README #

This project is basically about fetching data from backendless server and adding some persistance to it.

# Libraries Used #
1. RxJava2 and RxAndroid
2. Retrofit2
3. Gson
4. Room
5. Glide
6. Parcelable
7. Butterknife

# Few Screenshots #
![screenshot_20171218-213659](https://user-images.githubusercontent.com/9425999/34115441-c4043830-e43d-11e7-98a8-7fd13d00ec86.png)
![screenshot_20171218-212800](https://user-images.githubusercontent.com/9425999/34115460-ce639398-e43d-11e7-8442-77bd224fd124.png)
![screenshot_20171218-212806](https://user-images.githubusercontent.com/9425999/34115461-cea57d30-e43d-11e7-8e2c-67944d68b095.png)
